#include <stdio.h>
#include "image_ppm.h"
#include <math.h>

int main(int argc, char *argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille, seuil1, seuil2;

    if (argc != 5)
    {
        printf("Usage: ImageIn.pgm ImageOut.pgm seuil1 seuil 2\n");
        exit(1);
    }

    sscanf(argv[1], "%s", cNomImgLue);
    sscanf(argv[2], "%s", cNomImgEcrite);
    sscanf(argv[3], "%d", &seuil1);
    sscanf(argv[4], "%d", &seuil2);

    OCTET *ImgIn, *ImgOut, *ImgTemp;

    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);
    allocation_tableau(ImgTemp, OCTET, nTaille);

    for (int i = 0; i < nH; i++)
    {
        for (int j = 0; j < nW; j++)
        {
            if (ImgIn[i * nW + j] >= seuil2)// application seuil haut 
            {
                ImgTemp[i * nW + j] = 255;
            }
        }
    }
    for (int i = 0; i < nH; i++)
    {
        for (int j = 0; j < nW; j++) // parcour image
        {
            if (ImgTemp[i * nW + j] ==0 && ImgIn[i * nW + j] > seuil1) // dans l'intersection  des seuils
            {
                for (int a = -1; a < 2; a++)
                {
                    for (int b = -1; b < 2; b++) 
                    {
                        if (i + a >= 0 && i + a < nH && j + b >= 0 && j + b < nW)//test voisins
                        {
                            if (ImgTemp[(i + a) * nW + j + b] == 255)
                            {
                                ImgTemp[i * nW + j] = 255;
                            }
                        }
                    }
                }
            }
            ImgOut[i * nW + j] = ImgTemp[i * nW + j];
        }
    }

    ecrire_image_pgm(cNomImgEcrite, ImgOut, nH, nW);
    free(ImgIn);
    free(ImgOut);
    free(ImgTemp);

    return 1;
}
