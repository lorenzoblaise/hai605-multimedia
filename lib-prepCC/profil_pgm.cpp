#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
    char cNomImgLue[250];
    char cColLine[15];
    int nH, nW, nTaille, indice;
    
    if (argc != 4) {
        printf("Usage: ImageIn.pgm [Line|Col] indice\n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cColLine) ;
    sscanf (argv[3],"%d",&indice) ;

    if (strcmp(cColLine, "col") && strcmp(cColLine, "line")) {
        printf("écrire <<col>> ou <<line>>\n");
        exit(1);
    }

    OCTET *ImgIn;
    int *Profil;
    int size;

    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
	printf("\n");

    // lecture - comptage des niveaux de gris
    if (strcmp(cColLine, "col") == 0) {
        allocation_tableau(Profil, int, nH);
        size = nH;
        for(int y=0; y<nH; y++) {
            Profil[y] = ImgIn[y*nW+indice];
        }
    } else {
        allocation_tableau(Profil, int, nW);
        size = nW;
        for(int x=0; x<nW; x++) {
            Profil[x] = ImgIn[indice*nW+x];
        }
    }
    //enregistrement des résultats
    for(int i=0; i<size; i++) {
        printf("%i %i\n",i,Profil[i]);
    }

    free(ImgIn); 
    free(Profil);
    
    return 1;
}