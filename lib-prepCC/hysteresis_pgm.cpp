#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille, seuilHaut, seuilBas;
    
    if (argc != 5) {
        printf("Usage: ImageIn.pgm ImageOut.pgm seuilBas, seuilHaut\n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cNomImgEcrite);
    sscanf(argv[3],"%d",&seuilBas);
    sscanf(argv[4],"%d",&seuilHaut);

    OCTET *ImgIn, *ImgInter, *ImgOut;
    
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgInter, OCTET, nTaille);
    allocation_tableau(ImgOut, OCTET, nTaille);

    for(int i=0; i<nH; i++) {
        for(int j=0; j<nW; j++) {
            int val = ImgIn[i*nW+j];
            int ret = 0;
            if(val >= seuilHaut) {
                ret = 255;
            }
            ImgInter[i*nW+j] = ret;
        }
    }
    for(int i=0; i<nH; i++) {
        for(int j=0; j<nW; j++) {
            int valInter = ImgInter[i*nW+j];
            int valIn = ImgIn[i*nW+j];
            int ret = valInter;
            if(valInter == 0 && valIn >= seuilBas) {
                for(int a=-1; a<2; a++) {
                    for(int b=-1; b<2; b++) {
                        if(i+a>=0 && i+a<nH && j+b>=0 && j+b<nW) {
                            if(ImgInter[(i+a)*nW+j+b] == 255) {
                                ret = 255;
                            }
                        }
                    }
                }
            }
            ImgOut[i*nW+j] = ret;
        }
    }

    ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
    free(ImgIn); free(ImgInter); free(ImgOut);

    return 1;
}