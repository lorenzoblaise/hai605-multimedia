#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille, k;
    
    if (argc != 4) {
        printf("Usage: ImageIn.pgm ImageOut.pgm -128<k<128\n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue);
    sscanf (argv[2],"%s",cNomImgEcrite);
    sscanf (argv[3],"%d",&k);

    OCTET *ImgIn, *ImgInter, *ImgOut;
    
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgInter, OCTET, nTaille);
    allocation_tableau(ImgOut, OCTET, nTaille);

    for (int i=0; i<nW*nH; i++) {
        int val = ImgIn[i];
        val += k;
        if (val < 0) val = 0;
        if (val > 255) val = 255;
        ImgOut[i] = val;
    }

    ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
    free(ImgIn); free(ImgInter); free(ImgOut);

    return 1;
}