#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille;
    
    if (argc != 3) {
        printf("Usage: Image1.pgm Image2.pgm\n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cNomImgEcrite);

    OCTET *Img1, *ImgInter, *Img2;

    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    
    allocation_tableau(Img1, OCTET, nTaille);
    allocation_tableau(Img2, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, Img1, nH * nW);
    lire_image_pgm(cNomImgEcrite, Img2, nH * nW);
    allocation_tableau(ImgInter, OCTET, nTaille);

    double somme = 0;
    for(int i=0; i<nH; i++) {
        for(int j=0; j<nW; j++) {
            somme += (Img1[i*nW+j]-Img2[i*nW+j]) * (Img1[i*nW+j]-Img2[i*nW+j]);
        }
    }
    somme = somme/(nH*nW);
    printf("EQM : %f\n", somme);

    free(Img1); free(ImgInter); free(Img2);

    return 1;
}