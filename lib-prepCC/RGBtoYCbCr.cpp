#include <stdio.h>
#include "image_ppm.h"

void RGBtoYCbCr(OCTET* ImgIn, OCTET* ImgOut, int nW, int nH, int composante) {
    for(int i=0; i<nH; i++) {
        for(int j=0; j<nW; j++) {
            int r = ImgIn[3*(i*nW+j)];
            int g = ImgIn[3*(i*nW+j)+1];
            int b = ImgIn[3*(i*nW+j)+2];
            int y = (int)(0 + 0.299*r + 0.587*g + 0.114*b);
            int cb = (int)(128 - 0.1687236*r - 0.331264*g + 0.5*b);
            int cr = (int)(128 + 0.5*r -0.418688*g - 0.081312*b);
            int val;
            if (composante == 1) val = y;
            if (composante == 2) val = cb;
            if (composante == 3) val = cr;
            if (val < 0) val = 0;
            if (val > 255) val = 255;
            ImgOut[i*nW+j] = val;
        }
    }
}

int main(int argc, char* argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille;
    char y[] = "img/y.pgm";
    char cb[] = "img/cb.pgm";
    char cr[] = "img/cr.pgm";
    
    if (argc != 2) {
        printf("Usage: ImageIn.ppm\n"); 
        exit (1) ;
    }
    
    sscanf (argv[1],"%s",cNomImgLue);

    OCTET *ImgIn, *ImgInter, *ImgOut;
    
    lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;
    
    allocation_tableau(ImgIn, OCTET, nTaille*3);
    lire_image_ppm(cNomImgLue, ImgIn, nTaille);
    allocation_tableau(ImgInter, OCTET, nTaille);
    allocation_tableau(ImgOut, OCTET, nTaille);

    RGBtoYCbCr(ImgIn, ImgOut, nW, nH, 1);
    ecrire_image_pgm(y, ImgOut, nH, nW);
    RGBtoYCbCr(ImgIn, ImgOut, nW, nH, 2);
    ecrire_image_pgm(cb, ImgOut, nH, nW);
    RGBtoYCbCr(ImgIn, ImgOut, nW, nH, 3);
    ecrire_image_pgm(cr, ImgOut, nH, nW);

    free(ImgIn); free(ImgInter); free(ImgOut);

    return 1;
}