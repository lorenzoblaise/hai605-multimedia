#include <stdio.h>
#include "image_ppm.h"
#include <math.h>

int main(int argc, char *argv[])
{
    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille, val1, val2, gH, gV;

    if (argc < 3)
    {
        printf("Usage: ImageIn.pgm ImageOut.pgm \n");
        exit(1);
    }

    sscanf(argv[1], "%s", cNomImgLue);
    sscanf(argv[2], "%s", cNomImgEcrite);

    OCTET *ImgIn, *ImgOut;

    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);

    for (int i = 0; i < nH; i++)
    {
        for (int j = 0; j < nW; j++)
        {
            gH = 0;
            gV = 0;
            if (i > 0)
            {
                gV -= ImgIn[(i - 1) * nW + j];
            }
            else
            {
                gV -= ImgIn[i * nW + j];
            }
            if (i < nH - 1)
            {
                gV += ImgIn[(i + 1) * nW + j];
            }
            else
            {
                gV += ImgIn[i * nW + j];
            }
            if (j > 0)
            {
                gH -= ImgIn[i * nW + j - 1];
            }
            else
            {
                gH -= ImgIn[i * nW + j];
            }
            if (j < nW - 1)
            {
                gH += ImgIn[i * nW + j + 1];
            }
            else
            {
                gH += ImgIn[i * nW + j];
            }
            ImgOut[i * nW + j] = sqrt(gV * gV + gH * gH);
        }
    }
    ecrire_image_pgm(cNomImgEcrite, ImgOut, nH, nW);
    free(ImgIn);
    free(ImgOut);

    return 1;
}
