// floute une image en faisant la moyenne de ce pixel et de ces 4 voisins

#include <stdio.h>
#include "image_ppm.h"


int main(int argc, char* argv[])
{
	char cNomImgLue[250],cNomImgO[250];
	int nH, nW,nTaille,sum;

	if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm  \n"); 
       exit (1) ;
     }

     sscanf (argv[1],"%s",cNomImgLue) ;
     sscanf (argv[2],"%s",cNomImgO) ;

     OCTET *ImgIn;
     OCTET *ImgO;
     lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   	 nTaille = nH * nW;
  
   	 allocation_tableau(ImgIn, OCTET, nTaille);
    allocation_tableau(ImgO, OCTET, nTaille);
   	 lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

   for (int i=0; i < nTaille; i++)
    ImgO[i]= ImgIn[i];

   	 for (int i=1; i < nH-1; i++){
   	 	for (int j=1; j < nW-1; j++)
      	 {
          sum=ImgIn[i*nW+j];
          sum+=ImgIn[(i+1)*nW+j]+ImgIn[(i-1)*nW+j]+ImgIn[i*nW+j+1]+ImgIn[i*nW+j-1]; // addition des voinsins directes
      	  ImgO[i*nW+j]=sum/5;      	 
      	}}

    ecrire_image_pgm(cNomImgO, ImgO,  nH, nW);
    free(ImgIn);free(ImgO);
    return 1;
}