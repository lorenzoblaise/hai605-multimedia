//inverse les niveaux de gris d'une image

#include <stdio.h>
#include "image_ppm.h"



int main(int argc, char* argv[])
{
	char cNomImgLue[250],cNomImgO[250];
	int nH, nW,nTaille;

	if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm  \n"); 
       exit (1) ;
     }

     sscanf (argv[1],"%s",cNomImgLue) ;
     sscanf (argv[2],"%s",cNomImgO) ;

     OCTET *ImgIn;
     OCTET *ImgO;
     lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   	 nTaille = nH * nW;
  
   	 allocation_tableau(ImgIn, OCTET, nTaille);
    allocation_tableau(ImgO, OCTET, nTaille);
   	 lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

   	 for (int i=0; i < nH; i++){
   	 	for (int j=0; j < nW; j++)
      	 {
      	 	ImgO[i*nW+j]=255-ImgIn[i*nW+j];
      	 
      	}}

    ecrire_image_pgm(cNomImgO, ImgO,  nH, nW);
    free(ImgIn);free(ImgO);
    return 1;
}