// histo.cpp : tableau des niveaux de gris

#include <stdio.h>
#include "image_ppm.h"
#include <iostream>
#include <fstream>

int main(int argc, char* argv[])
{
	char cNomImgLue[250];
	int nH, nW,nTaille,histo_r[256],histo_g[256],histo_b[256];
	for (int i=0;i<256;i++){//init histo couleurs
        histo_r[i]=0;
        histo_g[i]=0;
        histo_b[i]=0;
        }
	if (argc != 2) 
     {
       printf("Usage: ImageIn.pgm \n"); 
       exit (1) ;
     }

     sscanf (argv[1],"%s",cNomImgLue) ;

     OCTET *ImgIn;
     lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   	 nTaille = nH * nW;
     int nTaille3 = nTaille * 3;

   	 allocation_tableau(ImgIn, OCTET, nTaille3);
   	 lire_image_ppm(cNomImgLue, ImgIn, nH * nW);

   	 for (int i=0; i < nTaille3; i+=3){
      	 
      	 	histo_r[ImgIn[i]]++;
            histo_g[ImgIn[i+1]]++;
             histo_b[ImgIn[i+2]]++;
      	 
      	}
      	
    free(ImgIn);
    std::ofstream f("histo-c.dat");
    for (int i=0;i<256;i++){
    	f <<i<< " " <<histo_r[i]<<" "<<histo_g[i+1]<<" " <<histo_b[i+2] <<"\n";
    }
    
	
    return 1;
}