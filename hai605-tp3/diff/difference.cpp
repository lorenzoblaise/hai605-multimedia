#include <stdio.h>
#include "image_ppm.h"
#include "dilatation.h"


int main(int argc, char* argv[])
{
  char imageseuil[250], cNomImgEcrite[250],imagedilate[250]="d.pgm";
  int nH, nW, nTaille;

  if (argc != 3) 
     {
       printf("Usage: imagain.pgm sortie.pgm \n"); 
       exit (1) ;
     }

    sscanf(argv[1],"%s",imageseuil);
   sscanf (argv[2],"%s",cNomImgEcrite);
  
  dila(imageseuil);
 

   OCTET *Seuil, *ImgOut,*Dilate;

   lire_nb_lignes_colonnes_image_pgm(imageseuil, &nH, &nW);
   nTaille = nH * nW;

   allocation_tableau(Seuil, OCTET, nTaille);
   lire_image_pgm(imageseuil, Seuil, nH * nW);
    allocation_tableau(Dilate, OCTET, nTaille);
   lire_image_pgm(imagedilate, Dilate, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);



  for (int i = 0; i < nH ; i++){
    for (int j = 0; j < nW ; j++){
        if(Seuil[i*nW+j]==Dilate[i*nW+j])ImgOut[i*nW+j]=255;
    }
  }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(Seuil); free(ImgOut);free(Dilate);

   return 1;
}