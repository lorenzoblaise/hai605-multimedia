

void dila( char cNomImgLue[250])
{
  int nH, nW, nTaille;


   OCTET *ImgIn, *ImgOut;

   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;

   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);



  for (int i = 1; i < nH - 1; i++){
    for (int j = 1; j < nW - 1; j++){
      if(ImgIn[i*nW+j] == 255){

      ImgOut[i*nW+j]=255;
      ImgOut[i*nW+j+1]=255;
      ImgOut[i*nW+j-1]=255;
      ImgOut[(i+1)*nW+j-1]=255;
      ImgOut[(i-1)*nW+j-1]=255;
      ImgOut[(i+1)*nW+j+1]=255;
      ImgOut[(i-1)*nW+j+1]=255;
      ImgOut[(i+1)*nW+j]=255;
      ImgOut[(i-1)*nW+j]=255;
      }
    }
  }

   ecrire_image_pgm("d.pgm", ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);


}