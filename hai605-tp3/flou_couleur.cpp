#include<iostream>
#include <stdio.h>
#include "image_ppm.h"


int main(int argc, char* argv[])
{
	char cNomImgLue[250],cNomImgO[250];
	int nH, nW,nTaille,sumr,sumg,sumb;

	if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm  \n"); 
       exit (1) ;
     }
 
     sscanf (argv[1],"%s",cNomImgLue) ;
     sscanf (argv[2],"%s",cNomImgO) ;

     OCTET *ImgIn;
     OCTET *ImgO;
     OCTET *r, *g ,*b,*r1,*g1,*b1;
     lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   	 nTaille = nH * nW;
   int nTaille3 =nTaille*3;
   	allocation_tableau(ImgIn, OCTET, nTaille3);
    allocation_tableau(r, OCTET, nTaille);
    allocation_tableau(g, OCTET, nTaille);
    allocation_tableau(b, OCTET, nTaille);
    allocation_tableau(r1, OCTET, nTaille);
    allocation_tableau(g1, OCTET, nTaille);
    allocation_tableau(b1, OCTET, nTaille);
    allocation_tableau(ImgO, OCTET, nTaille3);
   	lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
 
  planR(r,ImgIn,nTaille);//separation des couleurs
  planV(g,ImgIn,nTaille);
  planB(b,ImgIn,nTaille);


   for (int i=0; i < nTaille; i++){//remplissage contour couleur
    r1[i]= r[i];
    g1[i]=g[i];
    b1[i]=b[i];
}

for (int i=0; i < nTaille3; i++)
{   ImgO[i]= ImgIn[i];}

  for (int i=1; i < nH-1; i++){
    for (int j=1; j < nW-1; j++)
      {
      sumr=0;
      sumb=0;
      sumg=0; //remise a zero
      for(int x =-1;x<2;x++){
          for (int y=-1; y<2 ; y++){
          sumr+=r[(i+x)*nW+j+y];
          sumg+=g[(i+x)*nW+j+y];
          sumb+=b[(i+x)*nW+j+y];
        }}
        r1[i*nW +j]=sumr/9;
        g1[i*nW +j]=sumg/9;
        b1[i*nW +j]=sumb/9;
        }
      }
  for (int i=0; i<nTaille3;i+=3){
    ImgO[i]=r1[i/3];
    ImgO[i+1]=g1[i/3];
    ImgO[i+2]=b1[i/3];
  }   	

    ecrire_image_ppm(cNomImgO, ImgO,  nH, nW);
    free(ImgIn);free(ImgO);
    return 1;
}