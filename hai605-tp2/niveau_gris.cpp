#include <stdio.h>
#include <iostream>
#include "image_ppm.h"


int pixel_min(OCTET nom[],int x,int y);
int pixel_maxi(OCTET nom[],int x,int y);


int main(int argc, char* argv[])
{
    char cNomImgLue[250];
  int nH, nW, nTaille;

  if (argc != 2) 
     {
       printf("Usage: ImageIn.pgm  \n"); 
       exit (1) ;
     }

   sscanf (argv[1],"%s",cNomImgLue) ;
   
  

   OCTET *ImgIn, *ImgOut,*ImgOut2;

   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;

   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
   allocation_tableau(ImgOut2, OCTET, nTaille);

for (int i =1; i<nW-1;i++){
    for (int j =1; j<nH-1;j++){
        ImgOut[i*nW +j ]= pixel_min(ImgIn,i,j);
        ImgOut2[i*nW +j ]= pixel_maxi(ImgIn,i,j);
    }
}

   ecrire_image_pgm("erosion_gris.pgm", ImgOut,  nH, nW);
   ecrire_image_pgm("dilatation_gris.pgm", ImgOut2,  nH, nW);
   free(ImgIn); free(ImgOut);free(ImgOut2);
   return 1;
}


int pixel_min(OCTET nom[],int x,int y){
    int mini=255;
    for(int dx=-1 ;dx<2; dx++){
        for (int dy=-1; dy<2 ;dy++){
            if (nom[(x +dx) * 256+ y +dy]<mini){
                mini=nom[(x +dx) * 256+ y +dy];
            }
        }
    }
    return mini;
}

int pixel_maxi(OCTET nom[],int x,int y){
    int maxi=0;
    for(int dx=-1 ;dx<2; dx++){
        for (int dy=-1; dy<2 ;dy++){
            if (nom[(x +dx) * 256+ y +dy]>maxi){
                maxi=nom[(x +dx) * 256+ y +dy];
            }
        }
    }
    return maxi;
}