// Otsu dans une image 256 x 256
#include "histo.h"


int main(int argc, char* argv[])
{
    int hist[256],nH,nW;
    int  sum=0,q1=0,q2=0,sumB=0, varmax=0, thres=0;
     unsigned int m1,m2,sig ;
    char cNomImgLue[250],cNomImgEcrite[250];

     if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cNomImgEcrite);


    OCTET *ImgIn, *ImgOut;
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    int nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);

    for (int i=0;i<255;i++)hist[i]=0;
    histo (cNomImgLue,hist);// remplissage histo
   

    for (int i=0;i<255;i++){// calcul sum
        sum+= i * hist[i];
    }
   
    for (int t=0; t<255;t++){// calcul du threshold
    q1+=hist[t];
    

        if(q1>0 ){
        q2= nTaille -q1;
        sumB+= t * hist[t];
        m1= (sumB/q1);
        m2= (sum-sumB)/q2;

        sig= q1 * q2 /100 * ((m1-m2)*(m1-m2));
        
        if (sig > varmax){
            thres = t;
            varmax= sig;
        }
       }
    
    }
    std::cout<< thres;
    for (int i=0; i < nH; i++)
    for (int j=0; j < nW; j++)
     {
       if ( ImgIn[i*nW+j] > thres) ImgOut[i*nW+j]=255;
       else ImgOut[i*nW+j]=0;
     }



   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);
   
    return 0;
}