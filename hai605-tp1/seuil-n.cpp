

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, S1,S2;


  
  if (argc < 4) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm  Seuil1 Seuil2 ....Seuiln \n"); 
       exit (1) ;
     }
   int seuil[argc-3];// tableau de taille nb de seuil
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
  for (int i=3; i<argc ;i++){// on rempli le tableau
    sscanf(argv[i],"%d",&seuil[i-3]);
  }

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

 for (int i=0; i < nH; i++)
   for (int j=0; j < nW; j++){
    sscanf (argv[3],"%d",&S1);
    sscanf (argv[argc-1],"%d",&S2);

    if (ImgIn[i*nW+j] < S1) ImgOut[i*nW+j]=0;// test premier seuil
    else if (ImgIn[i*nW+j] > S2) ImgOut[i*nW+j]=255;// test dernier seuil

    else for (int k=4;k<argc;k++)
     {
      sscanf (argv[k-1],"%d",&S1);
      sscanf (argv[k],"%d",&S2);
      if (ImgIn[i*nW+j] < S2) ImgOut[i*nW+j]=(S2+S1)/2;
      break;// si on trouve on sort 
     }
   }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}
