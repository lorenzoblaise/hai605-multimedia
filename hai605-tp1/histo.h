// histo.cpp : tableau des niveaux de gris

#include <stdio.h>
#include "image_ppm.h"
#include <iostream>
#include <fstream>

void histo(char cNomImgLue[256],int hist[])
{
	int nH, nW,nTaille;
     OCTET *ImgIn;
     lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   	 nTaille = nH * nW;
  
   	 allocation_tableau(ImgIn, OCTET, nTaille);
   	 lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

   	 for (int i=0; i < nH; i++){
   	 	for (int j=0; j < nW; j++)
      	 {
      	 	hist[ImgIn[i*nW+j]]++;
      	 }
      	}
      	
    free(ImgIn);
    
}
